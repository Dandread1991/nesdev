.include "constants.inc"

.segment "ZEROPAGE"
.importzp player_x, player_y

.segment "CODE"
.import main
.export reset_handler
.proc reset_handler
    SEI
    CLD
    LDX #$00
    STX PPUCTRL
    STX PPUMASK

vblankwait:
    BIT PPUSTATUS
    BPL vblankwait

    LDX #$00
    LDA #$ff

clear_oam:
    STA $0200, X ;set sprite y-pos off screen
    INX
    INX
    INX
    INX
    BNE clear_oam

vblankwait:
    BIT PPUSTATUS
    BPL vblankwait

    ;zero-page values
    LDA #$80
    STA player_x
    LDA #$a0
    STA player_y
    JMP main
.endproc
