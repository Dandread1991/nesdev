.include "constants.inc"

.segment "ZEROPAGE"
.importzp letter_x, letter_y

.segment "CODE"
.import main
.export reset_handler
.proc reset_handler
    SEI
    CLD
    LDX #$00
    STX PPUCTRL
    STX PPUMASK

vblankwait:
    BIT PPUSTATUS
    BPL vblankwait

    LDX #$00
    LDA #$ff

clear_oam:
    STA $0200, X ;set sprite y-pos off screen
    INX
    INX
    INX
    INX
    BNE clear_oam

vblankwait2:
    BIT PPUSTATUS
    BPL vblankwait2


    ;zero-page values
    LDA #$80
    STA letter_x
    LDA #$a0
    STA letter_y
    JMP main

.endproc
