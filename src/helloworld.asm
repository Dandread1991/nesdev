.include "constants.inc"
.include "header.inc"

.segment "ZEROPAGE"
player_x: .res 1
player_y: .res 1
player_dir: .res 1
.exportzp player_x, player_y

.segment "CODE"
.proc irq_handler
    ; LDA #$00
    ; STA OAMADDR
    ; LDA #$02
    RTI
.endproc

.proc nmi_handler
    LDA #$00
    STA OAMADDR
    LDA #$02
    STA OAMDMA
    LDA #$00

    INC player_y
    ;update tiles after dma transfer
    JSR update_player

    JSR draw_player


    STA $2005
    STA $2005
    RTI
.endproc


.import reset_handler

.export main

.proc main
;write palette
    LDX PPUSTATUS
    LDX #$3f
    STX PPUADDR
    LDX #$00
    STX PPUADDR

load_palettes:
    LDA palettes, X
    STA PPUDATA
    INX
    CPX #$20
    BNE load_palettes


;load_sprites:
   ; LDA sprites, X
  ;  STA $0200, X   ; increment address and x
 ;   INX
 ;   CPX #$10
  ;  BNE load_sprites


    LDX #$00

;nametable


load_background:
    LDA PPUSTATUS
    LDA background, X
    STA PPUADDR
    INX
    LDA background, X
    STA PPUADDR
    INX
    LDA background, X
    STA PPUDATA
    INX
    CPX #$1e
    BNE load_background


    LDX $00







;attribute table
    LDA PPUSTATUS
    LDA #$23
    STA PPUADDR
    LDA #$d2
    STA PPUADDR
    LDA #%00001111
    STA PPUDATA
    LDA PPUSTATUS
    LDA #$23
    STA PPUADDR
    LDA #$d3
    STA PPUADDR
    LDA #%00000011
    STA PPUDATA
    LDA PPUSTATUS
    LDA #$23
    STA PPUADDR
    LDA #$d5
    STA PPUADDR
    LDA #%00001111
    STA PPUDATA
    LDA PPUSTATUS
    LDA #$23
    STA PPUADDR
    LDA #$d6
    STA PPUADDR
    LDA #%00000011
    STA PPUDATA

vblankwait:         ;wait for vblank
    BIT PPUSTATUS
    BPL vblankwait

    LDA #%10010000      ;turn on NMI, sprites use first pattern table
    STA PPUCTRL
    LDA #%00011110      ;turn on screen
    STA PPUMASK

forever:
    JMP forever
.endproc

.proc update_player
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA player_x
  CMP #$e0
  BCC not_at_right_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$00
  STA player_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_right_edge:
  LDA player_x
  CMP #$10
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$01
  STA player_dir   ; start moving right
direction_set:
  ; now, actually update player_x
  LDA player_dir
  CMP #$01
  BEQ move_right
  ; if player_dir minus $01 is not zero,
  ; that means player_dir was $00 and
  ; we need to move left
  DEC player_x
  JMP exit_subroutine
move_right:
  INC player_x
exit_subroutine:
  ; all done, clean up and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

;sprites:
;.byte $70, $05, $00, $80
;.byte $70, $06, $00, $87
;.byte $77, $07, $00, $80
;.byte $77, $08, $00, $87

background:
.byte $21, $08, $0b
.byte $21, $09, $08
.byte $21, $0a, $0f
.byte $21, $0b, $0f
.byte $21, $0c, $12 ;Hello
.byte $21, $14, $1a
.byte $21, $15, $12
.byte $21, $16, $15
.byte $21, $17, $0f
.byte $21, $18, $07 ;World



.proc draw_player
  ; save registers
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  ; write player ship tile numbers
  LDA #$05
  STA $0201
  LDA #$06
  STA $0205
  LDA #$07
  STA $0209
  LDA #$08
  STA $020d

  ; write player ship tile attributes
  ; use palette 0
  LDA #$00
  STA $0202
  STA $0206
  STA $020a
  STA $020e

  ; store tile locations
  ; top left tile:
  LDA player_y
  STA $0200
  LDA player_x
  STA $0203

  ; top right tile (x + 8):
  LDA player_y
  STA $0204
  LDA player_x
  CLC
  ADC #$08
  STA $0207

  ; bottom left tile (y + 8):
  LDA player_y
  CLC
  ADC #$08
  STA $0208
  LDA player_x
  STA $020b

  ; bottom right tile (x + 8, y + 8)
  LDA player_y
  CLC
  ADC #$08
  STA $020c
  LDA player_x
  CLC
  ADC #$08
  STA $020f

  ; restore registers and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

.segment "VECTORS"
.addr nmi_handler, reset_handler, irq_handler

.segment "RODATA"
palettes:
.byte $0f, $12, $23, $27
.byte $0f, $2b, $3c, $39
.byte $0f, $0c, $07, $13
.byte $0f, $19, $09, $29

.byte $0f, $2d, $10, $15
.byte $0f, $19, $09, $29
.byte $0f, $19, $09, $29
.byte $0f, $19, $09, $0f

.segment "CHR"
.incbin "graphics.chr"
