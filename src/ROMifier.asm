.include "constants.inc"
.include "header.inc"
.include "converted_text.inc"
;.import famistudio_ca65.s

tile_start = $0201
; .incbin "JourneyToSilius.nsf"
; .define FAMISTUDIO_CA65_ZP_SEGMENT   ZP
; .define FAMISTUDIO_CA65_RAM_SEGMENT  RAM
; .define FAMISTUDIO_CA65_CODE_SEGMENT PRG

.segment "ZEROPAGE"
letter_x: .res 1
letter_y: .res 1
letter_dir: .res 1
letter_x2: .res 1
letter_y2: .res 1
letter_x3: .res 1
letter_y3: .res 1
letter_x4: .res 1
letter_y4: .res 1
letter_x5: .res 1
letter_y5: .res 1
letter_x6: .res 1
letter_y6: .res 1
scroll: .res 1
ppuctrl_settings: .res 1
.exportzp letter_x, letter_y, letter_dir
letter2_x = $56
letter2_y = $13
letter3_x = $16
letter3_y = $1f
letter4_x = $76
letter4_y = $05
letter5_x = $0f
letter5_y = $63
letter6_x = $f3
letter6_y = $e3

.segment "CODE"
.proc irq_handler
     LDA #$00
     STA OAMADDR
     LDA #$02
    RTI
.endproc

.proc nmi_handler
    LDA #$00
    STA OAMADDR
    LDA #$02
    STA OAMDMA
    LDA #$00


    ;update tiles after dma transfer

    JSR draw_tile
    DEC letter_y
   ; JSR update_tile_sprites




    JSR update_tile
    DEC letter_x
    JSR update_tile2
    DEC letter3_y
    JSR update_tile3
     DEC letter4_y
    JSR update_tile4
    INC letter5_x
    DEC letter5_y
    JSR update_tile5
    DEC letter6_x
    JSR update_tile6

    DEC letter2_y
    DEC letter3_y
    DEC letter4_y
    DEC letter5_y
    DEC letter6_y
    INC letter_x
    INC letter4_y
    JSR update_tile4
    DEC letter2_x
    JSR update_tile2


     LDA scroll
	CMP #$00 ; did we scroll to the end of a nametable?
	BNE set_scroll_positions
	; if yes,
	; Update base nametable
	LDA ppuctrl_settings
	EOR #%00000010 ; flip bit 1 to its opposite
	STA ppuctrl_settings
	STA PPUCTRL
	LDA #240
	STA scroll

    set_scroll_positions:
	LDA #$00 ; X scroll first
	STA PPUSCROLL
	DEC scroll
	LDA scroll ; then Y scroll
	STA PPUSCROLL



   ; STA $2005
   ; STA $2005
    RTI
.endproc

.import reset_handler
.import draw_starfield
.import draw_objects

.export main

.proc main
;sound data
  ; lda #%00000111  ;enable Sq1, Sq2 and Tri channels
   ;  sta $4015

;     ;Square 1
;     lda #%00111000  ;Duty 00, Volume 8 (half volume)
;     sta $4000
;     lda #$C9        ;$0C9 is a C# in NTSC mode
;     sta $4002       ;low 8 bits of period
;     lda #$00
;     sta $4003       ;high 3 bits of period

;     ;Square 2
;     lda #%01110110  ;Duty 01, Volume 6
;     sta $4004
;     lda #$A9        ;$0A9 is an E in NTSC mode
;     sta $4006
;     lda #$00
;     sta $4007

;     ;Triangle
;     lda #%10000001  ;Triangle channel on
;     sta $4008
;     lda #$42        ;$042 is a G# in NTSC mode
;     sta $400A
;     lda #$00
;     sta $400B
;famistudio_init

;JSR silius

LDA #239	 ; Y is only 240 lines tall!
	STA scroll
;write palette
    LDX PPUSTATUS
    LDX #$3f
    STX PPUADDR
    LDX #$00
    STX PPUADDR

load_palettes:
    LDA palettes, X
    STA PPUDATA
    INX
    CPX #$20
    BNE load_palettes

    LDX #$00

    LDX #$20
	JSR draw_starfield

	LDX #$28
	JSR draw_starfield

	JSR draw_objects

    vblankwait:         ;wait for vblank
    BIT PPUSTATUS
    BPL vblankwait

    LDA #%10010000      ;turn on NMI, sprites use first pattern table
    STA ppuctrl_settings
    STA PPUCTRL
    LDA #%00011110      ;turn on screen
    STA PPUMASK

forever:
   JMP forever
.endproc

 .proc draw_tile
  ; save registers
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA


  ; write  tile numbers
    LDX #$00
    LDY #$04

    load_tiles:

    LDA tiles_inc, X
    STA tile_start
    INX
    LDA tiles_inc, X
    STA tile_start, Y
    INX
    LDA tiles_inc, X
        inc_Y:

        INY

        CPY #$08
        BNE inc_Y
    STA tile_start, Y
    INX
    LDA tiles_inc, X
    inc_Y2:

        INY

        CPY #$0c
        BNE inc_Y2
    STA tile_start, Y
    INX
    LDA tiles_inc, X
    INY
    INY
    INY
    INY
    STA tile_start, Y
    INX
    LDA tiles_inc, X
    INY
    INY
    INY
    INY
    STA tile_start, Y
    INX
   ;CPX #$02
  ; BNE load_tiles

  ; write  tile attributes
  ; use palette 0,1,2



  LDA #$00
  STA $0202
  LDA #$07
  STA $0206
  LDA #$11
  STA $020a
  LDA #$13
  STA $020e
  LDA #$02
  STA $0212
  LDA #$1a
  STA $0216

;   ; store tile locations
  ; first:
  LDA letter_y
  STA $0200
  LDA letter_x
  STA $0203


  ; second (x + 8):
  LDA letter2_y
  STA $0204
  LDA letter2_x
  CLC
  ADC #$08
  STA $0207

   ; third (x + 16):
  LDA letter3_y
  STA $0208
  LDA letter3_x
  CLC
  ADC #$10
  STA $020b

   ; fourth (x + 24)
  LDA letter4_y
  STA $020c
  LDA letter4_x
  CLC
  ADC #$18
  STA $020f
   ;fifth (x+32)
  LDA letter5_y
  STA $0210
  LDA letter5_x
  CLC
  ADC #$20
  STA $0213

  ; sixth (x+40)
  LDA letter6_y
  STA $0214
  LDA letter6_x
  CLC
  ADC #$28
  STA $0217


  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
 .endproc

update_tile_sprites:
    sprite_ram = $0200
    LDA sprite_ram
    CLC
    ADC #$08
    STA sprite_ram
    CLC
    ADC #$08
    STA sprite_ram+3
    STA sprite_ram+8
    DEX
    STA sprite_ram+12

    LDA sprite_ram+3
    STA sprite_ram+11
    CLC
    ADC #$08
    STA sprite_ram+7
    STA sprite_ram+15

    RTS

.proc update_tile
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA letter_x
  CMP #$e0
  BCC not_at_right_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$00
  STA letter_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_right_edge:
  LDA letter_x

  CMP #$10
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$01
  STA letter_dir   ; start moving right
direction_set:
  ; now, actually update letter_x
  LDA letter_dir
  CMP #$01
  BEQ move_right
  ; if letter_dir minus $01 is not zero,
  ; that means letter_dir was $00 and
  ; we need to move left
  DEC letter_x

  JMP exit_subroutine
move_right:
  INC letter_x


exit_subroutine:
  ; all done, clean up and return

  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

.proc update_tile2
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA letter2_x
  CMP #$e0
  BCC not_at_left_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$01
  STA letter_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_left_edge:
  LDA letter2_x

  CMP #$00
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$00
  STA letter_dir   ; start moving right
direction_set:
  ; now, actually update letter_x
  LDA letter_dir
  CMP #$00
  BEQ move_left
  ; if letter_dir minus $01 is not zero,
  ; that means letter_dir was $00 and
  ; we need to move left
  DEC letter2_x

  JMP exit_subroutine
move_left:
  INC letter2_x
LDA #$00
   STA $0206
exit_subroutine:
  ; all done, clean up and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

.proc update_tile3
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA letter3_x
  CMP #$e0
  BCC not_at_left_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$01
  STA letter_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_left_edge:
  LDA letter3_x

  CMP #$00
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$00
  STA letter_dir   ; start moving right
direction_set:
  ; now, actually update letter_x
  LDA letter_dir
  CMP #$00
  BEQ move_left
  ; if letter_dir minus $01 is not zero,
  ; that means letter_dir was $00 and
  ; we need to move left
  DEC letter3_x

  JMP exit_subroutine
move_left:
  INC letter3_x

exit_subroutine:
  ; all done, clean up and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

.proc update_tile4
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA letter4_x
  CMP #$e0
  BCC not_at_left_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$01
  STA letter_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_left_edge:
  LDA letter4_x

  CMP #$00
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$00
  STA letter_dir   ; start moving right
direction_set:
  ; now, actually update letter_x
  LDA letter_dir
  CMP #$00
  BEQ move_left
  ; if letter_dir minus $01 is not zero,
  ; that means letter_dir was $00 and
  ; we need to move left
  DEC letter4_x

  JMP exit_subroutine
move_left:
  INC letter4_x

exit_subroutine:
  ; all done, clean up and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

.proc update_tile5
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA letter5_x
  CMP #$e0
  BCC not_at_left_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$01
  STA letter_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_left_edge:
  LDA letter5_x

  CMP #$00
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$00
  STA letter_dir   ; start moving right
direction_set:
  ; now, actually update letter_x
  LDA letter_dir
  CMP #$00
  BEQ move_left
  ; if letter_dir minus $01 is not zero,
  ; that means letter_dir was $00 and
  ; we need to move left
  DEC letter5_x

  JMP exit_subroutine
move_left:
  INC letter5_x
LDA #$29
  STA $0212
exit_subroutine:
  ; all done, clean up and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

.proc update_tile6
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA

  LDA letter6_x
  CMP #$e0
  BCC not_at_left_edge
  ; if BCC is not taken, we are greater than $e0
  LDA #$01
  STA letter_dir    ; start moving left
  JMP direction_set ; we already chose a direction,
                    ; so we can skip the left side check
not_at_left_edge:
  LDA letter6_x

  CMP #$00
  BCS direction_set
  ; if BCS not taken, we are less than $10
  LDA #$00
  STA letter_dir   ; start moving right
direction_set:
  ; now, actually update letter_x
  LDA letter_dir
  CMP #$00
  BEQ move_left
  ; if letter_dir minus $01 is not zero,
  ; that means letter_dir was $00 and
  ; we need to move left
  DEC letter6_x

  JMP exit_subroutine
move_left:
  INC letter6_x

exit_subroutine:
  ; all done, clean up and return
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS
.endproc

explode:
    PHP
  PHA
  TXA
  PHA
  TYA
  PHA

    LDA  #$2e
    STA tile_start

  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTS



.segment "VECTORS"
.addr nmi_handler, reset_handler, irq_handler

.segment "RODATA"
;.import tiles_inc
palettes:
.byte $0f, $12, $23, $27   ;background
.byte $0f, $2b, $3c, $39
.byte $0f, $0c, $07, $13
.byte $0f, $19, $09, $29

.byte $0f, $00, $10, $20    ;sprite
.byte $0f, $01, $11, $21
.byte $0f, $0c, $16, $13
.byte $0f, $19, $09, $29



.segment "CHR"
.incbin "ascii.chr"
