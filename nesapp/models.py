from django.db import models

# Create your models here.
class NESRomModel(models.Model):
    name = models.CharField(max_length=100)
    text_box = models.CharField(max_length=20)

    def __str__(self):
        return self.name
