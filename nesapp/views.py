from django.shortcuts import render,redirect
from nesapp.forms import NESUploadForm, ConvertForm
from nesapp.models import NESRomModel
from nesapp import NESRomify

# Create your views here.
def show_main(request):
    return render(request, "nesapp/main.html")


def NesRomUploadView(request):
    if request.method=="POST":
        item_form = NESUploadForm(request.POST)

        if item_form.is_valid():
            item = item_form.save()
            item.save()

            NESRomify.NESRomify(item.text_box)
           # NESRomify.remove_asm(item.name)
            return redirect("show_main")
    else:
        item_form = NESUploadForm()
    context = {
        "form": item_form,
    }
    return render(request, "nesapp/nes_upload.html",context)

def romListView(request):
    rom_list = NESRomModel.objects.all()
    context = {
        "rom_list": rom_list
    }
    return render(request, "nesapp/rom_list.html",context)

def ConvertFormView(request):
    if request.method == "POST" and "run_batch" in request.POST:
        NESRomify.run_BAT_create_NES()
        convert_form = ConvertForm()
        convert_item = "None"
        context = {
        "convert_form": convert_form,
        "converted_text":convert_item
        }
        return render(request,"nesapp/convert.html",context)

    elif request.method == "POST":
        convert_form = ConvertForm(request.POST)

        if convert_form.is_valid():
            convert_item_text = convert_form.cleaned_data["text_to_convert"]

            convert_item_HEX=NESRomify.text_to_HEX(convert_item_text)
            convert_item_BIN=NESRomify.text_to_BIN(convert_item_text)
            NESRomify.NESRomify(convert_item_text)
            context = {
                "convert_form": convert_form,
                "converted_text_HEX":convert_item_HEX,
                "converted_text_BIN":convert_item_BIN,
            }
            return render(request,"nesapp/convert.html",context)
    else:
        convert_form = ConvertForm()
        convert_item = "None"
    context = {
        "convert_form": convert_form,
        "converted_text":convert_item
    }
    return render(request, "nesapp/convert.html",context)
