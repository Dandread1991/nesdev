from django.apps import AppConfig


class NesappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nesapp'
