from django import forms
from nesapp.models import NESRomModel

class NESUploadForm(forms.ModelForm):
    class Meta:
        model = NESRomModel
        fields = ["name","text_box"]

class ConvertForm(forms.Form):
    text_to_convert = forms.CharField(max_length=20)
