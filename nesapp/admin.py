from django.contrib import admin
from nesapp.models import NESRomModel

# Register your models here.
@admin.register(NESRomModel)
class NESRomAdmin(admin.ModelAdmin):
    list_display = [ "name",]
