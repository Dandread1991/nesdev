from nesapp.models import NESRomModel
from nesapp.forms import NESUploadForm
from pprint import pprint
import os
from pathlib import Path
import subprocess

def NESRomify(nes_form_text):

    #print(f"Creating file {nes_form.name}.asm ")
    romfile = open("../nesdev/src/converted_text.inc", "w")
    romfile.write('.segment "RODATA" \n tiles_inc: \n .byte ')

    romfile.writelines((text_to_HEX(nes_form_text)))
    romfile.close()


def remove_asm(filename):
    print(f"Deleting {filename}")
    filepath = f"{filename}.asm"
    os.remove(filepath)


def text_to_HEX(text_str):
    converted_data = []
   # converted_data.append('.segment "RODATA" \n')
   # converted_data.append('tiles_inc:\n')
    #converted_data.append(".byte ")
    for ch in text_str:
        converted_data.append("$")
        converted_data.append(hex(ord(ch)).lstrip("0x"))
        converted_data.append(", ")
    converted_data=converted_data[:-1]
    return converted_data


def text_to_BIN(text_str):
    converted_data = []
    for ch in text_str:

        converted_data.append(bin(ord(ch)).lstrip("0b"))
    return converted_data


def run_BAT_create_NES():

    os.system('assemble_nes.bat')
