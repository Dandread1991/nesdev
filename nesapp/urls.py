from django.urls import path
from nesapp.views import show_main, NesRomUploadView, romListView, ConvertFormView

urlpatterns = [
    path("",show_main, name="show_main"),
    path("nes_upload/", NesRomUploadView, name="nes_upload"),
    path("rom_list/", romListView, name="rom_list"),
    path("convert/",ConvertFormView, name="convert")
]
