# NesDev





NES Romifier

## Description
This is a simple, currently very alpha implementation of a text-to-6502 ASM tool/toy. It serves no real purpose other than to showcase my attempts to learn the basics of assembly programming and Nintendo Entertainment System development. That said, the code is functional and I'll be iterating and refactoring as a I go along.


## Installation
As of this first implementation, the user will be required to install their own NES emulator in order to run the outputted files, which will be .NES files created in the project directory.

## Usage
Type in a string, currently limited 6 or fewer characters. The program will output a NES rom file which can be loaded into an emulator to display the created graphics.




## Roadmap
There's a lot I'd like to do to fine tune this alpha:
    -shift to a more robust front end
    -integrate an emulator so user is not required to move outside of this app
    -improve and refactor code to allow for more possible outputs
    -implement sound engine to generate music based on user input
    -algorithmic graphic generation rather than hard coded (because of limits in NES architecture, this may not be possible)
    -possibly port code to C/C++ to more quickly add new features



## Contributing
As this is a personal/learning project, I'm not looking for any code contributions as this time, though I am always up for discussing and brainstorming


## Project status
Slowly moving forward...
